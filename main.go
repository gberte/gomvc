package main

import (
	"net/http"

	"./config"
	"./helpers"
	"./router"
	_ "github.com/go-sql-driver/mysql"
)

func bootstrap() {

	//Initializing configuration
	config.Params.Init()
	helpers.db.Init(config.Params.ConfigurationString)
	r := router.Router.Init()

	//Initializing the server
	http.ListenAndServe(":3000", r)
}

func main() {
	bootstrap()
}
