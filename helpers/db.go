package helpers

import (
	"log"

	"github.com/jinzhu/gorm"
)

var Db *gorm.DB

func InitDB(configurationString string) {
	var err error
	Db, err = gorm.Open("mysql", configurationString)
	if err != nil {
		log.Fatal(err)
	}
}
