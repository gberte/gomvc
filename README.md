
# Overview
* Simple MVC structured (Models, Views, Controllers) scafold project
* Free to use and modify or expand

## Motivation
Just for fun, learning the language

## Installation
Download the project, install all the dependencies and run it.

# Dependencies
* [gorm](https://github.com/jinzhu/gorm)
* [chi](https://github.com/go-chi/chi)
* [gonfig](https://github.com/tkanos/gonfig)

# Author
Gustavo Mario Bertenasco

# License
This project is released under the MIT licence. See [LICENSE](LICENSE) for more details.