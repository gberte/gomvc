package controllers

import (
	"encoding/json"
	"html/template"
	"log"
	"net/http"
	"strconv"
	"time"

	"../helpers"
	"../models"
	"github.com/go-chi/chi"
)

type UsersController struct{}

func getID(w http.ResponseWriter, r *http.Request) uint {
	//Getting the id
	ids := chi.URLParam(r, "id")
	auxid, err2 := strconv.ParseUint(ids, 10, 32)
	if err2 != nil {
		log.Println(err2)
	}
	return uint(auxid)
}

func (wc UsersController) List(w http.ResponseWriter, r *http.Request) {
	user := models.User{}
	type Data struct {
		Title string
		Rows  []models.User
	}
	t, _ := template.ParseFiles("./views/users.html")
	t.Execute(w, &Data{Title: "Usuarios", Rows: user.List()})
}

func (wc UsersController) Get(w http.ResponseWriter, r *http.Request) {
	id := getID(w, r)
	user := models.User{}
	user.Load(id)
	type Data struct {
		Title string
		Body  []byte
	}

	t, _ := template.ParseFiles("./views/user.html")
	t.Execute(w, &Data{Title: user.Username})
}

func (wc UsersController) GetJson(w http.ResponseWriter, r *http.Request) {
	id := getID(w, r)
	user := models.User{}
	user.Load(id)
	type Data struct {
		Title string
		Body  []byte
	}

	json.NewEncoder(w).Encode(&user)
}

func (wc UsersController) Save(w http.ResponseWriter, r *http.Request) {
	id := getID(w, r)

	//Validating data
	user := models.User{}
	val := helpers.Validate(r, user.GetRules())
	if len(val) == 0 {

		//Saving object
		if id != 0 {
			//Searching the object to update
			user.Load(id)
		}

		//Setting the values
		roleid, err := strconv.ParseInt(r.FormValue("roleid"), 10, 64)
		if err != nil {
			log.Println(err)
		}
		active, err := strconv.ParseBool(r.FormValue("active"))
		if err != nil {
			log.Println(err)
		}
		user.Username = r.Form.Get("username")
		user.Name = r.Form.Get("name")
		user.Email = r.Form.Get("email")
		user.Password = r.Form.Get("password")
		user.RoleID = int(roleid)
		user.Active = active
		user.CreatedAt = time.Now()
		user.Save()

		//Creating the view
		type Data struct {
			Title string
			Body  []byte
		}
		t, _ := template.ParseFiles("./views/user.html")
		t.Execute(w, &Data{Title: user.Username})
	} else {
		//Return 400

	}
}

func (wc UsersController) Delete(w http.ResponseWriter, r *http.Request) {
	id := getID(w, r)
	if id != 0 {
		user := models.User{}
		user.Load(id)
		user.Delete()
	}
}
