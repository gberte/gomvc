package controllers

import (
	"html/template"
	"net/http"
)

type IndexController struct{}

func (i IndexController) Main(w http.ResponseWriter, r *http.Request) {
	type Data struct {
		Title string
		Body  []byte
	}
	t, _ := template.ParseFiles("./views/index.html")
	t.Execute(w, &Data{Title: "Home"})
}

func (i IndexController) Welcome(w http.ResponseWriter, r *http.Request) {
	type Data struct {
		Title string
		Body  []byte
	}
	t, _ := template.ParseFiles("./views/index.html")
	t.Execute(w, &Data{Title: "Bienvenido"})
}
