package router

import (
	"context"
	"net/http"
	"os"
	"path/filepath"
	"strings"
	"time"

	"../controllers"
	"github.com/go-chi/chi"
	"github.com/go-chi/chi/middleware"
	_ "github.com/go-sql-driver/mysql"
)

func IdCtx(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		ctx := context.WithValue(r.Context(), "id", chi.URLParam(r, "id"))
		next.ServeHTTP(w, r.WithContext(ctx))
	})
}

// FileServer conveniently sets up a http.FileServer handler to serve
// static files from a http.FileSystem.
func FileServer(r chi.Router, path string, root http.FileSystem) {
	if strings.ContainsAny(path, "{}*") {
		panic("FileServer does not permit URL parameters.")
	}

	fs := http.StripPrefix(path, http.FileServer(root))

	if path != "/" && path[len(path)-1] != '/' {
		r.Get(path, http.RedirectHandler(path+"/", 301).ServeHTTP)
		path += "/"
	}
	path += "*"

	r.Get(path, http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		fs.ServeHTTP(w, r)
	}))
}

type RoutersManager struct{}

var Router RoutersManager

func (c RoutersManager) Init() chi.Router {

	//Creating the router
	r := chi.NewRouter()
	r.Use(middleware.Timeout(60 * time.Second))

	//Static content router
	workDir, _ := os.Getwd()
	filesDir := filepath.Join(workDir, "public")
	FileServer(r, "/", http.Dir(filesDir))

	//Initializing APP paths
	index := controllers.IndexController{}
	r.Get("/", index.Main)
	r.Get("/2", index.Welcome)
	users := controllers.UsersController{}
	r.Route("/users", func(r chi.Router) {
		r.Get("/", users.List)
		r.Route("/{id}", func(r chi.Router) {
			r.Use(IdCtx)
			r.Get("/", users.Get)
			r.Put("/", users.Save)
			r.Delete("/", users.Delete)
		})
		r.Route("/json/{id}", func(r chi.Router) {
			r.Use(IdCtx)
			r.Get("/", users.GetJson)
		})
	})

	return r

}
