package config

import (
	_ "github.com/go-sql-driver/mysql"
)

type Configuration struct {
	ConfigurationString string
	Routers             string
}

var Params Configuration

func (c Configuration) Init() {
	Params = Configuration{}

	//Development
	Params.ConfigurationString = "root:foo@/kala?charset=utf8&parseTime=True&loc=Local"
}
