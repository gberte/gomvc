package models

import (
	"log"

	"../helpers"
	"github.com/jinzhu/gorm"
)

type User struct {
	gorm.Model
	Username string `json:"username,omitempty"`
	Name     string `json:"name,omitempty"`
	Email    string `json:"email,omitempty"`
	Password string `json:"password,omitempty"`
	RoleID   int    `json:"roleid,omitempty"`
	Active   bool   `json:"active,omitempty"`
}

func (u *User) GetRules() map[string]string {
	var rules map[string]string
	rules = make(map[string]string)
	rules["Username"] = ""
	rules["Name"] = ""
	rules["Email"] = ""
	rules["Password"] = ""
	rules["RoleID"] = ""
	rules["Active"] = ""
	return rules
}

func (u *User) List() []User {
	defer helpers.Db.Close()
	rows, err := helpers.Db.Model(&User{}).Rows() // (*sql.Rows, error)
	if err != nil {
		log.Fatal(err)
	}
	defer rows.Close()
	var users = make([]User, 0)
	for rows.Next() {
		var err error
		user := User{}
		err = rows.Scan(&user.ID, &user.Username, &user.Name, &user.Email, &user.Password, &user.RoleID, &user.Active, &user.CreatedAt, &user.UpdatedAt, &user.DeletedAt)
		if err != nil {
			log.Fatal(err)
		}
		users = append(users, user)
	}
	return users
}

func (u *User) Load(id uint) {
	defer helpers.Db.Close()
	helpers.Db.First(&u, id)
}

func (u *User) Save() {
	defer helpers.Db.Close()
	if helpers.Db.NewRecord(u) {
		helpers.Db.Create(&u)
	} else {
		helpers.Db.Save(&u)
	}
}

func (u *User) Delete() {
	defer helpers.Db.Close()
	if u.ID != 0 {
		helpers.Db.Delete(&u)
	}
}
